BINARNY STROM:
-------------
`Pomocou štruktúry binárny strom navrhnite počítačovú hru, ktorá formou konverzácie s počítačom umožní určiť hľadané zviera, ktoré si hráč práve myslí. Počítač kladie otázky hráčovi, ktorý odpovedá áno alebo nie.`

PRIKLAD:
--------

lieta hľadané zviera?
            áno / \nie
        vták           šteká?
                    áno / \ nie
                    pes         bučí?
                            áno / \ nie
                          krava     ryb

`Pridajte možnosť priebežného pridávania ďaľších zvierat, ukladania hry do súboru a načítanie hry zo súboru`

Ďalej program umožní používateľovi interaktívne používať operácie údajového typu binárny strom, ktorý v príklade používate. Zahrňte následujúce možnosti v rámci interakcie s užívateľom

- a. Create – vytvorenie prázdneho stromu
- b. Make – pridanie vrchola stromu ako nasledovníka aktívneho vrchola
- c. Root – nastaví aktívny prvok na koreň stromu
- d. Lchild – nastaví aktívny prvok na ľavého nasledovníka
- e. Rchild – nastaví aktívny prvok na pravého nasledovníka
- f. Parent – nastaví aktívny prvok na predchodcu aktívneho vrchola
- g. Data – vráti hodnotu aktívneho vrchola
- h. načítaj binárny strom zo súboru
- i. zapíš binárny strom do súboru
- j. aký vták je reprezentovaný daným stromom?
- ... sem prídu ďaľšie možnosti podľa potreby x... Ukonči program.


`Tento program opakovane vypíše svoju ponuku (menu) pre užívateľa na obrazovku. Vstupom do tohto programu je jedna z hore uvedených možnosti a-j, x zadaná užívateľom cez klávesnicu v rámci nekonečnej slučky až kým užívateľ neukončí program cez voľbu ’x’. Program po načítaní danej voľby prevedie príslušnú operáciu a vypíše svoj výstup na monitor a do textového súboru vystup.txt`

Vyžaduje sa dôsledné používanie tried v C++ a hlavičkového súboru **<iostream.h>** pre vstup z klávesnice resp. **<fstream.h>** pre výstup do súboru. Hlavnou dátovou štruktúrov programu bude ADT binárny strom (binTree) presne tak, ako bol špecifikovaný na prednáškach. Jeho špecifikácia sa bude nachádzať v hlavičkovom súbore: **binTree.h** a jeho implementácia v súbore: **binTree.cpp**. Naviac dátová (private) časť binárneho stromu bude implementovaná použitím smerníkov a dynamickej pamäte. Musia sa výhradne používať iba operácie binárneho stromu špecifikované v súbore **binTree.h** pri práci s binárnym stromom. Vyžaduje sa správny štýl programovania a v prípade potreby aj primeraný komentár, resp. príslušné chybové hlásenie. Program bude navrhnutý ako čisto objektovo orientovaný a musí byť funkčný, a zároveň riadne otestovaný v rámci svojho main-programu t.j. testovacieho drajveru **animal.cpp**


ODOVZAJTE:
----------

- 1. odladený zdrojový kód: binTree.h, binTree.cpp a animal.cpp
- 2. textový súbor s vytlačeným zoznamom vystup.txt
- 3. predviesť na cvičení – platí iba pre denných študentov
